<?php
/**
 * Created by PhpStorm.
 * User: apinchuk
 * Date: 7/22/16
 * Time: 10:51 AM
 */
if (array_key_exists('action', $_POST) && $_POST['action'] == 'order' ) {
    if(!isset($_POST['name']) || empty($_POST['name']) || !isset($_POST['phone']) || empty($_POST['phone']))
    {
        header('Location: index.html?msg=operation_fail');
    }

    $city = (isset($_POST['city']))?$_POST['city']:'';

    $to = 'aleksandr.pinchuk@sannacode.com';
    $subject = 'Order form '.$_SERVER['HTTP_REFERER'];
    $subject = "=?utf-8?b?". base64_encode($subject) ."?=";
    $message = "Имя: ".$_POST['name']."\nТелефон: ".$_POST['phone'].
                "\nГород: $city".
                "\nIP: ".$_SERVER['REMOTE_ADDR'];
    $headers = 'Content-type: text/plain; charset="utf-8"';
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Date: ". date('D, d M Y h:i:s O') ."\r\n";

    mail($to, $subject, $message, $headers);
    header('Location: index.html?msg=mail_sent');
    exit;
} else {
    header('Location: index.html?msg=operation_fail');
    exit;
}
?>