// JavaScript Document
$(window).scroll(function() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
});
//jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    var $root = $('body, html');
    var res = getParameterByName('msg');
    if(res){
        if(res == 'mail_sent'){
            alert('Спасибо! Наш менеджер скоро свяжется с Вами!')
        } else {
            alert('Произошла обишка! Попробуйте ещё раз.');
        }
        window.location.search = '';
    }
    $('nav a').on('click', function() {
        var navbarHeight = $('.navbar').outerHeight() > 100 ? 86 : $('.navbar').outerHeight();

        if($.attr(this, 'href') != '#'){
            $('#navbarCollapse').collapse('hide');
            $root.animate({
                scrollTop: $( $.attr(this, 'href') ).offset().top - navbarHeight
            }, 500);    
        }
        return false;
    });

    $(".btn:not(button[type=submit])").on('click', function(){
        var navbarHeight = $('.navbar').outerHeight() > 100 ? 86 : $('.navbar').outerHeight();

        $('#timerModal').modal('hide');
        $root.animate({
            scrollTop: $('.form-box').offset().top - navbarHeight
        }, 500);    
    });
    $("#myCarousel").swiperight(function() {  
      $("#myCarousel").carousel('prev');  
    });  
    $("#myCarousel").swipeleft(function() {  
      $("#myCarousel").carousel('next');  
    });  
    $(window).bind('scroll', function () {
        $('#bs-example-navbar-collapse-1').collapse('hide');
    });


    // setTimeout(function(){
    //     $('#timerModal').modal();
    // }, 1000);

    function getTimeRemaining(endtime) {
        var t = Date.parse(endtime) - Date.parse(new Date());
        var seconds = Math.floor((t / 1000) % 60);
        var minutes = Math.floor((t / 1000 / 60) % 60);
        var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
        var days = Math.floor(t / (1000 * 60 * 60 * 24));
        return {
            'total': t,
            'days': days,
            'hours': hours,
            'minutes': minutes,
            'seconds': seconds
        };
    }

    function initializeClock(id, endtime) {
        var clock = document.getElementById(id);
        var daysSpan = clock.querySelector('.days');
        var hoursSpan = clock.querySelector('.hours');
        var minutesSpan = clock.querySelector('.minutes');
        var secondsSpan = clock.querySelector('.seconds');

        function updateClock() {
            var t = getTimeRemaining(endtime);

            daysSpan.innerHTML = ('0' + t.days).slice(-2);
            hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
            minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
            secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

            if (t.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        updateClock();
        var timeinterval = setInterval(updateClock, 1000);
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
    Date.prototype.addDays = function(days) {
        this.setDate(this.getDate() + parseInt(days));
        return this;
    };
    var deadline = new Date();
    deadline.addDays(2);
    initializeClock('clockdiv', deadline);
});
// Closes the Responsive Menu on Menu Item Click
 $(function () { $('.navbar-collapse ul li a:not(.dropdown-toggle)').click(function () { $('.navbar-toggle:visible').click(); }); });